<?php
//error_reporting(0);
error_reporting(E_ALL);

function db(){

	$conn = new Mongo('localhost');
	$db = $conn->utrack;
	return $db ;
}

// @vikrant  function for creating log for testing 
function logToFile($filename, $msg) { 
        // open file
        $fd = fopen($filename, "a");
        // append date/time to message
        $str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg; 
        // write string
        fwrite($fd, $str . "\n");
        // close file
        fclose($fd);

 }
?>