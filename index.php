<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();


require 'include/routes.php';
require 'include/db.php';
require 'include/pushnotify.php';

// @vikrant Api to Register New User 

function registerNewUser(){
    
    $db = db();
    $collection = $db->users;
    if(isset($_POST["user_name"]) && isset($_POST["phn_no"]) && isset($_POST["pass"]) && isset($_POST["lat"]) && isset($_POST["lng"]) && isset($_POST["gcm_id"])) {

        $criteria_user = array("phn_no"=>$_POST["phn_no"]);
        $user = $collection->findOne($criteria_user);
        if(!count($user)){
            $_POST["visibility_status"] = 1;
            $insert = $collection->insert($_POST);
            if($insert){
                print_r(json_encode(array("response"=>"User Created")));
            }else{
                print_r(json_encode(array("response"=>"User Not Created")));
            }
        }else{
            print_r(json_encode(array("response"=>"User Already Present")));
        }   
    }else{
        print_r(json_encode(array("response"=>"Invalid Parameters")));
    }
}

// @vikrant Api to Update image for registerd user
// Base64 will be converted to image Url 
// and will be saved in the database

function registerNewUserImage(){
    
    $db = db();
    $collection = $db->users;
    
    if(isset($_POST["phn_no"]) && isset($_POST["image"])){

        $criteria_user = array("phn_no"=>$_POST["phn_no"]);
        $user = $collection->findOne($criteria_user);
        if(count($user)){   

            $image_url = 'images/'.$_POST["phn_no"].'.png';
            $newdata = array('$set' => array("image"=>$image_url));
            $update = $collection->update(array("phn_no"=>$_POST["phn_no"]),$newdata);
            header('Content-Type: bitmap; charset=utf-8');
            file_put_contents($image_url, base64_decode($_POST["image"]));

            if($update){
                print_r(json_encode(array("response"=>"Image Updated")));
            }else{
                print_r(json_encode(array("response"=>"Image Not Updated")));
            }
        }else{
            print_r(json_encode(array("response"=>"User Not Present")));
        } 

    }else{

        print_r(json_encode(array("response"=>"Invalid Parameters")));   
    }
}

// @vikrant Api for Checking and Sending register Users 
// Back to device

function getRegisteredUsers(){
   
    $db = db();
    $collection = $db->users;
    
    if(isset($_POST["device_contact_list"])){
        $phn_nos = json_decode($_POST["device_contact_list"]);
        
        $confirmed_data = $collection->find(array(
                "phn_no" => array('$in'=>$phn_nos)
                ))->fields(array("phn_no" => true, "image"=>true,"user_name"=>true));

        foreach ($confirmed_data as $confirmed){
            $confirmed_ids[] = $confirmed;
        }
        if(count(@$confirmed_ids)){
            print_r(json_encode(array("confirmed_ids"=>$confirmed_ids)));
        }else{
            print_r(json_encode(array("response"=>"Data Not Present")));      
        }
    }else{
        print_r(json_encode(array("response"=>"Invalid Parameters")));      
    }
}

// @vikrant Api for sending request to users 
// Through Push Notifications
// will aslo create a event in a events for current event 
function sendRequestsToJoin(){

    $db = db();
    $collection_users = $db->users;
    $collection_events = $db->events;
    
    if(isset($_POST["device_request_list"]) && isset($_POST["from"])){

            $from = $_POST["from"];
            $event_id = $from."_".rand(99,1000).rand(99,1000);
            $event_maker_data = $collection_users->findOne(array("phn_no" => $from));

            if(count($event_maker_data)){
                $accepted_requests = array($event_maker_data["phn_no"]);
                $event_tbl_data = array(
                        "from"=>$event_maker_data["phn_no"],
                        "event_id"=>$event_id,
                        "accepted_nos"=>$accepted_requests
                        );
                $insert_event = $collection_events->insert($event_tbl_data);

                $phn_nos = json_decode($_POST["device_request_list"]);
                $confirmed_data = $collection_users
                    ->find(array("phn_no" => array('$in'=>$phn_nos)))
                    ->fields(array("gcm_id" => true,"phn_no"=>true));   
                foreach ($confirmed_data as $nos){
                    pushNotification($nos["gcm_id"],"You have a Request from ".$event_maker_data["user_name"],json_encode(array("event_id"=>$event_id)));
                }
                print_r(json_encode(array("response"=>"Completed")));             
            }else{
                print_r(json_encode(array("response"=>"You are Not Regitered")));             
            }
    }else{
        print_r(json_encode(array("response"=>"Invalid Parameters")));         
    }



}

// @sunny Api for  Confirmation from user 
// Through Push Notifications
function sendConfirmationToJoin(){

    try{
        $db= db();
        $collection_users = $db->users;
        $collection_events = $db->events;
        if(empty($_REQUEST)){
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
        if(isset($_REQUEST['message']) && isset($_REQUEST['confirmed_user_id']) && isset($_REQUEST['event_id'])){
            $events_user_confirm = array();
            $events_user_confirm_gcm_id =array();
            $message = $_REQUEST['message'];
            $confirmed_user_id = $_REQUEST['confirmed_user_id'];
            $event_id = $_REQUEST['event_id'];

            if(trim(strtolower($_REQUEST['message']))==="yes"){
                $check_event = $collection_events->findOne(array("event_id" => $event_id));
                if(isset($check_event)){
                    $check_confirm_user = $collection_users->findOne(array("phn_no"=>$confirmed_user_id));
                    if(isset($check_confirm_user)){
                            $from = $check_event['from'];
                            $accepted_nos= $check_event['accepted_nos'];
                            if(array_search($confirmed_user_id,$accepted_nos)=== false) {
                                array_push($accepted_nos,$confirmed_user_id);
                                $add_confirmed_id = $collection_events->update(array("event_id"=>$check_event['event_id']), array('$set' =>array("accepted_nos"=>$accepted_nos)));
                                if($add_confirmed_id){
                                    foreach ($accepted_nos as $ph_no) {
                                        $user_details = $collection_users->findOne(array("phn_no"=>$ph_no));              
                                        $user_info = array("user_name"=>$user_details['user_name'],"lat"=>$user_details['lat'],"lng"=>$user_details['lng'],"visibility_status"=>$user_details['visibility_status']); 
                                        array_push($events_user_confirm,$user_info);
                                        array_push($events_user_confirm_gcm_id,$user_details);
                                    }

                                    foreach ($events_user_confirm_gcm_id as $nos) {
                                        pushNotification($nos["gcm_id"],$nos["user_name"]." has joined the Event",json_encode($events_user_confirm));
                                    }
                                    print_r(json_encode(array("response"=>"Completed"))); exit;
                                }
                                else{
                                    print_r(json_encode(array("response"=>"Event not Created"))); exit;
                                }
                            }else{
                                print_r(json_encode(array("response"=>"Event Already Present"))); exit;
                            }
                    }else{
                        print_r(json_encode(array("response"=>"User not found"))); exit;
                    }
                }else{
                    print_r(json_encode(array("response"=>"Event not found"))); exit;
                }
            }
            else{
                print_r(json_encode(array("response"=>"Cancel Request"))); exit;
            }


        }else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
    }   catch(Exception $e){
            print_r(json_encode(array("Caught exception"=>$e->getMessage()))); exit;
    }    

}


// @sunny Api for  Get All Category from user 
function getCategoryToJoin(){
    try{
        $db = db();
        $collection_category = $db->category;
        $all_category = iterator_to_array($collection_category->find());
        if($all_category){
                print_r(json_encode(array("category"=>$all_category))); exit;
        }
        else{
            print_r(json_encode(array("response"=>"Category not found"))); exit;
        }
    }   catch (Exception $e) {
            print_r(json_encode(array("Caught exception"=>$e->getMessage()))); exit;
    }
}

// @sunny Api for user update
function updateUserInfo(){
    try{   
        $db =db();
        $collection_users = $db->users;
        
        if(isset($_REQUEST['user_name']) && isset($_REQUEST['phn_no']) && isset($_REQUEST['lat']) && isset($_REQUEST['lng']) && isset($_REQUEST['pass'])){
            $var =  \Slim\Slim::getInstance() -> request();
            $user_name =$var->params('user_name');
            $phn_no = $var->params('phn_no');
            $lat = $var->params('lat');
            $long = $var->params('lng');
            $pass = $var->params('pass');
            $check_confirm_user = $collection_users->findOne(array("phn_no"=>$phn_no));
            if($check_confirm_user){
                $user_profile_update = $collection_users->update(array("phn_no"=>$phn_no), array('$set' =>array("user_name"=>$user_name,"lat"=>$lat,"long"=>$long,"pass"=>$pass)));
                if($user_profile_update){
                    print_r(json_encode(array("response"=>"User updated"))); exit;
                }else
                {
                    print_r(json_encode(array("response"=>"User not update"))); exit;
                }
            }else{
                print_r(json_encode(array("response"=>"User not found"))); exit;
            }
        }
        else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
    }   catch(Exception $e){
            print_r(json_encode(array("Caught exception"=>$e->getMessage()))); exit;
    }
}


// @ sunny Api  cancel metting
function cancelMeeting(){
    try{
        $db = db();
        $collection_users = $db->users;
        $collection_events = $db->events;
        if(empty($_REQUEST)){
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
        if(isset($_REQUEST['event_id']) && isset($_REQUEST['from']) && isset($_REQUEST['message'])){
            $event_id = $_REQUEST['event_id'];
            $from = $_REQUEST['from'];
            $message = $_REQUEST['message'];
            $event_data_details = $collection_events->findOne(array("event_id"=>$event_id,"from"=>$from));
            if($event_data_details){
                foreach($event_data_details['accepted_nos'] as $acc_value){
                    $user_details = $collection_users->findOne(array("phn_no"=>$acc_value));
                    if($user_details){
                        pushNotification($user_details["gcm_id"],"this meeting has been canceled because ".$message,'Cancel Meeting');                         
                    }
                }
                $delete_event = $collection_events->remove(array("event_id"=>$event_data_details['event_id']));
                if($delete_event){
                    print_r(json_encode(array("response"=>"Event deleted")));
                }
            }else{
                print_r(json_encode(array("response"=>"Event not Found"))); exit;
            }
        }
        else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); 
        }
    }   catch(Exception $e){
            print_r(json_encode(array("Caught exception"=>$e->getMessage()))); exit; 
    }
}

// @ sunny api for leave meeting
function leaveMeeting(){
    try{
        $db = db();
        $collection_users = $db->users;
        $collection_events = $db->events;
        if(empty($_REQUEST)){
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
        if(isset($_REQUEST['phn_no']) && isset($_REQUEST['message']) && isset($_REQUEST['event_id'])) {
            $phn_no = $_REQUEST['phn_no'];
            $message = $_REQUEST['message'];
            $event_id = $_REQUEST['event_id'];
            $event_details = $collection_events->findOne(array("event_id"=>$event_id));
            if($event_details){
                $accepted_nos = $event_details['accepted_nos'];
                if(($key = array_search($phn_no,$accepted_nos)) !== false) {
                    foreach($accepted_nos as $acc_value){
                        $user_details = $collection_users->findOne(array("phn_no"=>$acc_value));
                        if($user_details){
                            pushNotification($user_details["gcm_id"], $user_details["user_name"]." has left the meeting because ".$message,'leave Meeting');                         
                        }
                    }
                    unset($accepted_nos[$key]);  
                    $user_leave = $collection_events->update(array("event_id"=>$event_id), array('$set' =>array("accepted_nos"=>$accepted_nos))); 
                    if($user_leave){
                        print_r(json_encode(array("response"=>$user_details["user_name"]." leave meeting"))); exit;
                    }
                }else{
                    print_r(json_encode(array("response"=>"Not in event"))); exit;
                }
            }else{
                print_r(json_encode(array("response"=>"Event not Found"))); exit;
            }
        }else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
    }   catch(Exception $e){
            print_r(json_encode(array("caught exception"=>$e->getMessage()))); exit;
    }
}


// @ sunny api for User Visibility
function userVisibility(){
    try{
        $db = db();
        $collection_users = $db->users;
        if(empty($_REQUEST)){
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
        if(isset($_REQUEST['phn_no'])){
            $phn_no = $_REQUEST['phn_no'];
            $user_details = $collection_users->findOne(array("phn_no"=>$phn_no));
            if($user_details){
                $visibility_status = ($user_details['visibility_status']==1?0:1);
                $visibility_change = $collection_users->update(array("phn_no"=>$phn_no),array('$set' =>array("visibility_status"=>$visibility_status)));
                print_r(json_encode(array("response"=>"Completed"))); exit;
            }else {
                print_r(json_encode(array("response"=>"User not found"))); exit;
            }
        }else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }                
    }   catch (Exception $e){
            print_r(json_encode(array("caught exception"=>$e->getMessage()))); exit;
    }
} 

// @ sunny api for location  update api 
function locationUpdate(){
    try{
        $db = db();
        $collection_users = $db->users;
        if(empty($_REQUEST)){
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
        if(isset($_REQUEST['phn_no']) && isset($_REQUEST['lat']) && isset($_REQUEST['lng'])){
            $phn_no = $_REQUEST['phn_no'];
            $lat = $_REQUEST['lat'];
            $lng = $_REQUEST['lng'];
            $user_details = $collection_users->findOne(array("phn_no"=>$phn_no));
            if($user_details){
               $update = $collection_users->update(array("phn_no"=>$phn_no),array('$set'=>array("lat"=>$lat,"lng"=>$lng)));
               if($update){
                    print_r(json_encode(array("response"=>"Updated successfully"))); exit;
               }
            }else{
                print_r(json_encode(array("response"=>"User not found"))); exit;
            }
        }else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
    }   catch(Exception $e){
            print_r(json_encode(array("caught exception"=>$e->getMessage()))); exit;
    }
}

// @ sunny api for All event user
function allEventUsers(){
    try{
        $db = db();
        $collection_users = $db->users;
        $collection_events = $db->events;
        $users_arr = array();
        if(empty($_REQUEST)){
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
        if(isset($_REQUEST['event_id'])){
            $event_id = $_REQUEST['event_id'];
            $event_details = $collection_events->findOne(array("event_id"=>$event_id));
            if($event_details){
                foreach ($event_details['accepted_nos'] as  $acc_value) {
                    $user_details = $collection_users->findOne(array("phn_no"=>$acc_value));
                    array_push($users_arr,array("user_name"=>$user_details['user_name'],"lat"=>$user_details['lat'],"lng"=>$user_details['lng'],"visibility_status"=>$user_details['visibility_status']));          
                }
                print_r(json_encode(array("event_users"=>$users_arr))); exit;
            }else{
                print_r(json_encode(array("response"=>"event not found"))); exit;
            }
        }
        else{
            print_r(json_encode(array("response"=>"Invalid Parameters"))); exit;
        }
    }   catch(Exception $e){
            print_r(json_encode(array("caught exception"=>$e->getMessage()))); exit;
    } 
}

$app->run();
